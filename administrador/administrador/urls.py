"""administrador URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from dashboard import views as dashboard_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', dashboard_views.login, name='login'),
    path('login/', dashboard_views.login, name='login'),
    path('logout/', dashboard_views.logout, name='logout'),
    path('dashboard/', dashboard_views.dashboard, name='dashboard'),
    path('cicloescolar/', dashboard_views.cicloescolar, name='cicloescolar'),
    path('cicloescolar/historico/', dashboard_views.cicloescolarHistorico, name='cicloescolarhistorico'),
    path('cicloescolar/agregar/', dashboard_views.cicloescolarInsert, name='cicloescolarinsert'),
    path('cicloescolar/editar/', dashboard_views.cicloescolarUpdate, name='cicloescolarupdate'),
    path('cicloescolar/eliminar/', dashboard_views.cicloescolarDelete, name='cicloescolardelete'),
    path('sucursales/', dashboard_views.sucursales, name='sucursales'),
    path('sucursales/agregar/', dashboard_views.sucursalesInsert, name='sucursalesinsert'),
    path('sucursales/editar/', dashboard_views.sucursalesUpdate, name='sucursalesupdate'),

    path('reportes/historico/', dashboard_views.historico, name='historico'),
    path('reportes/', dashboard_views.reporte, name='reportes'),
    path('tablagrupos/', dashboard_views.grupos, name='grupos'),
    path('grupos/agregar', dashboard_views.gruposInsert, name='gruposinsert'),
    path('grupos/editar', dashboard_views.gruposUpdate, name='modificar'),

    path('profesores/', dashboard_views.profesores, name='profesores'),
    path('profesores/agregar/', dashboard_views.profesoresInsert, name='profesoresinsert'),
    path('profesores/eliminar/',dashboard_views.profesoresDelete, name='profesoresdelete'),
    path('profesores/editar/',dashboard_views.profesoresUpdate, name='profesoresupdate'),

    path('alumnos/', dashboard_views.alumnos, name='alumnos'),
    path('alumnos/ver/', dashboard_views.alumnosView, name='alumnosview'),
    path('alumnos/agregar/', dashboard_views.alumnosInsert, name='alumnosinsert'),
    path('alumnos/editar/', dashboard_views.alumnosUpdate, name='alumnosupdate'),

    path('grupos/asistencia/', dashboard_views.asistencia, name='grupos/asistencia'),
    path('grupos/formulario/', dashboard_views.grupos_form, name='grupos_form'),
    path('grupos/agregar', dashboard_views.gruposInsert, name='gruposinsert'),
    path('grupos/editar', dashboard_views.gruposUpdate, name='modificar'),

    #path('/', ),
]
