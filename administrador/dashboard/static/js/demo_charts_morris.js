var morrisCharts = function() {

   Morris.Bar({
        element: 'morris-bar-example',
        data: [
            { y: '2019-A', a: 30},
            { y: '2019-B', a: 45 },
            { y: '2019-C', a: 20 }
        ],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Series A'],
        barColors: ['#B64645']
    });

    Morris.Line({
      element: 'morris-line-example',
      data: [
        { y: '2013', a: 50000 },
        { y: '2014', a: 75000 },
        { y: '2015', a: 50000 },
        { y: '2016', a: 85000 },
        { y: '2017', a: 65000 },
        { y: '2018', a: 90000 },
        { y: '2019', a: 100000 }
      ],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Ganancia'],
      resize: true,
      lineColors: ['green']
    });



 


    Morris.Donut({
        element: 'morris-donut-example',
        data: [
            {label: "Ramirez Perez Juan", value: 4},
            {label: "Gonzalez Mendoza Diana", value: 3},
            {label: "Solis Vega Mariana", value: 2},
            {label: "Toronja Rangel Fernando ", value: 3},
            {label: "Reyes García Jorge", value: 5}
        ],
        colors: ['#95B75D', '#1caf9a', '#FEA223','red','gray']
    });

}();