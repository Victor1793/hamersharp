# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as autlogin, logout as autlogout
from django.http import HttpResponseRedirect
from django.core.exceptions import *
#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------Variables Globales------------------------------------------------------------#

#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------Vistas------------------------------------------------------------------#
@csrf_protect
def login(request):
    data = {}
    try:
        if request.POST:
            usuario = request.POST['usuario']
            password = request.POST['password']
            usuarioEncontrado = User.objects.get(email=usuario) if '@' in usuario else User.objects.get(username=usuario)
            validPassword = usuarioEncontrado.check_password(password)
            if validPassword:
                autenticado = authenticate(username=usuarioEncontrado.username, password=password)
                if autenticado.is_active:
                    autlogin(request, autenticado)
                    return HttpResponseRedirect('/dashboard/')
                else:
                    data['mensaje'] = 'La cuenta de este usuario aún no se ha activado'
            else:
                data['mensaje'] = 'La contraseña no corresponde a ese usuario'
    except ObjectDoesNotExist:
        print("Excepcion en vista login: ObjectDoesNotExist")
        data['mensaje'] = "El correo no existe" if '@' in usuario else 'El usuario no existe'
    except Exception as ex:
        print("Excepcion en vista login: {}".format(type(ex).__name__))
    else:
        print("Sin excepciones en vista login")
    finally:
        print("Termina vista login")
    return render(request, 'login.html', data,)
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def logout(request):
    data = {}
    data['mensaje'] = 'Vuelve pronto'
    autlogout(request)
    return render(request, 'login.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def dashboard(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'inicio.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolar(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'calendarioEscolar.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarHistorico(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'historicoCalendarioEscolar.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarInsert(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'calendario_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarUpdate(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'calendario_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def cicloescolarDelete(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'calendario_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def sucursales(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'sucursales.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def sucursalesInsert(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'sucursales_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def sucursalesUpdate(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'sucursales_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def historico(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'historia.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url= '/login/')
def reporte(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'reporte.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposInsert(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'grupos.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def grupos(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'grupos_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposUpdate(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'modificar.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def profesores(request):
    data={}
    data['mensaje']=''
    return render(request,'profesores.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def profesoresInsert(request):
    data={}
    data['mensaje']=''
    return render(request,'agregarProfesor.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def profesoresDelete(request):
    data={}
    data['mensaje']=''
    return render(request,'eliminarProfesor.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def profesoresUpdate(request):
    data={}
    data['mensaje']=''
    return render(request,'agregarProfesor.html',data,)
#-----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def alumnos(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'alumnos.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#Ver Alumno#
@login_required(login_url = '/login/')
def alumnosView(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'alumno_ver.html', data, )

#----------------------------------------------------------------------------------------------------------------------#
#Agregar Alumno#
@login_required(login_url = '/login/')
def alumnosInsert(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'alumnos_form.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
#Modificar Alumno#
@login_required(login_url = '/login/')
def alumnosUpdate(request):
    data = {}
    data['mensaje'] = ''
    return render(request, 'alumnos_form_update.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def asistencia(request):
    data={}
    data['mensaje'] = ''
    return render(request, 'asistencias.html')
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url = '/login/')
def grupos_form(request):
    data={}
    data['mensaje'] = ''
    return render(request, 'grupos_asistencias_form.html')
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposInsert(request):
    data={}
    data['mensaje'] = ''
    return render(request, 'grupos.html', data, )
#----------------------------------------------------------------------------------------------------------------------#
@login_required(login_url='/login/')
def gruposUpdate(request):
    data={}
    data['mensaje'] = ''
    return render(request, 'modificar.html', data, )
#-----------------------------------------------------------------------------------------------------------------------#