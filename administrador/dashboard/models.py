from django.db import models
from django.contrib.auth.models import User

class MyModelName(models.Model):
    """
    Una clase típica definiendo un modelo, derivado desde la clase Model.
    """

